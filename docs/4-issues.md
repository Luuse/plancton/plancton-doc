---
title: Issues 
public: yes
---

## L'affichage des glyphes ne s'actualise pas à la réécriture des fichiers avec `ctrl` + `m`
Il peut s'agir d'une erreur dans le fichier `def.mp` : une fonction, une boucle mal fermée, ou bien une erreur dans le chemin de sortie des svg.

##La fonte ne s'exporte pas
Il peut s'agir d'un svg qui bloque l'export de la fonte. Dans ce cas, le terminal peut indiquer le svg à supprimer.
