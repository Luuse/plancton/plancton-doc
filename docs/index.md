
Plancton est environnement de développement destiné à réaliser des typographies numériques par le biais du langage de construction de figures [MetaPost](https://fr.wikipedia.org/wiki/MetaPost "metapost").

!["01presentation"](images/01.png)
*Meta-Old-French sur Plancton*

Le but de Plancton est de faciliter la visualisation et la génération de glyphes Metapost. Il permet la contextualisation d’une lettre dans un mot ou une phrase, d’éditer les fichiers de descriptions de glyphes ainsi que les fichiers de fonctions et de variables qui composent un projet. 

L’envie principale de ce projet n’est pas de rendre l’écriture du code metapost plus simple et plus «friendly», même s’il est proposé à la création d’un nouveau projet des fonctions par défauts (hints, grille, variable d’unités).

Si la création d’un tel environnement demande une certaine organisation de fichiers, l’envie reste avant tout de préserver au maximum une liberté dans l’écriture et l’organisation du code.

Plancton est une interface web basé sur le framework python [Bottle](https://bottlepy.org/docs/dev/) et utilise le module python [fontforge](https://fontforge.org/docs/scripting/python.html) pour la génération des fichiers `.otf` `.ttf` `.ufo`.


