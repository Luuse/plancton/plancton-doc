---
title: Installation
public: yes
---
## Dépendances
- Python2 ou Python3

- TexLive
	* Debian/Ubuntu : `sudo apt-get install texlive`
	* Arch/Manjaro : `sudo apt-get install texlive`
	* MacOs : Télécharger [macTex](https://www.tug.org/mactex/) et installer le paquet.
	* Windows : Télécharger  le fichier install-tl-windows.exe, cliquez avec le bouton droit sur le fichier et sélectionnez «Exécuter en tant qu'administrateur»

- Fontforge
	* Debian/Ubuntu : `sudo apt install python-fontforge`
	* Arch/Manjaro : `sudo pacman -S fontforge`
	* MacOs : cela se complique un peu. Il faut alors :
		- télécharger [Brew]("https://brew.sh/index_fr"), gestionnaire de paquets pour macOS : 
		`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"` 
		- `brew install fontforge` puis `brew link fontforge`
		- Mettre les dépendances fontforge dans les dépendances de la version de Python utilisée : 
		`sudo cp $(find $(brew --prefix fontforge)/. -name "fontforge.so") $(python3 -c "import site;print(site.getsitepackages()[0]);")`
		puis `sudo cp $(find $(brew --prefix fontforge)/. -name "psMat.so") $(python3 -c "import site;print(site.getsitepackages()[0]);")` 
	* Windows à venir !

- Inkscape
	* Linux : `sudo apt install inkscape`
	* [Mac](https://inkscape-manuals.readthedocs.io/en/latest/installing-on-mac.html#homebrew-and-macports) et [Windows](https://inkscape-manuals.readthedocs.io/en/latest/installing-on-windows.html)

- Virtualenv (optionnel)
	* Ubuntu : `sudo apt-get install virtualenv`
	* MacOs : `pip install virtualenv` (avec Python2) ou `pip3 install virtualenv` (Python3)

## Installation

![numero](images/1.svg) Télécharger Plancton [ici](https://gitlab.com/Luuse/plancton/plancton-editor) ou avec git : `git clone https://gitlab.com/Luuse/plancton/plancton-editor.git`

![numero](images/2.svg) Se rendre dans le dossier où se trouve Plancton : `cd plancton-editor`

### sans environnement

![numero](images/3.svg) Télécharger les modules Python :  `pip install -r requirements.txt` ou `pip3 install -r requirements.txt`


### avec un [environnement](https://docs.python.org/fr/3/tutorial/venv.html')
![numero](images/3.svg) `virtualenv plancton-env` et `source plancton-env/bin/activate`

![numero](images/4.svg) Télécharger les modules Python :  `pip install -r requirements.txt` ou `pip3 install -r requirements.txt`


## Lancer le serveur

`python plancton-server.py` (Python2) ou `python3 plancton-server.py` (Python3)

Le serveur est à l'adresse `http://localhost:8088/`. Pour quitter le serveur, appuyer sur la touche `Ctrl` + `C`.

(Si l'on souhaite utiliser un environnement après la première installation, il suffit de relancer l'environnement virtuel : `virtualenv plancton-env`, `source plancton-env/bin/activate` avant de lancer le serveur avec `python plancton-server.py` )





