---
title: Commandes & shortcuts
public: yes
---

Afin d'éviter de surcharger l'interface de menus et de boutons, les actions se font principalement par des commandes et des raccourcis clavier.

##Les raccourcis clavier
* Ré-écrire tous les SVG après modification du fichier `<global>.mp` : `ctrl` + `g`
* Ré-écrire le SVG après modification des fichiers `<key>.mp` et `def.mp`: `ctrl` + `m`

* Ouvrir/fermer le panneau de gauche : `ctrl` + `l`
* Ouvrir/fermer le panneau de droite : `ctrl` + `r`

## Les commandes {: .salut}

Les commandes se font via l'input de texte, qui devient un invité de commandes lorsque que l'on commence par `:`:

* Créer un nouveau project : ` :new project <project_name>`
* Ajouter un glyphe `:add <keycode>` (exemple: `:add 65` pour un A).
* Supprimer un glyph `:del <keycode>` (exemple: `:del 65` pour un A).
* Nettoyer l'ordre des variables (x,y,z,ze) d'un glyph `:clean <keycode>` (exemple: `:clean 65` pour un A).
* Affichier le spécimen `:specimen`
* Générer un fichier otf `:generate font`



