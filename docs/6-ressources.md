---
title: Ressources
public: yes
---

## Documentation Metapost
- [Metapost cheatset](https://gitlab.com/erg-type/workshop.meta-elastique/-/blob/master/cheatsheet/cheatsheet.md)
- [Un Manuel de METAPOST](https://melusine.eu.org/syracuse/metapost/f-mpman-2.pdf)
- [Metapost : exemples](https://perso.ensta-paris.fr/~diam/tex/online/tex.loria.fr_zoonekynd_metapost/)

## Metaphilie
- Une [conférence](https://anrt-nancy.fr/anrt-22/fr/journal/k-knuth) de Thomas Huot-Marchand
- Une conférence de David Reinfurt, [Mathematical Typography](https://www.youtube.com/watch?v=koc5MDoGW8U)
- Encore David Reinfurt : Mathematical Typography dans A *New* Program for Graphic Design, pp. 81 – 96
- Un [article](https://m-u-l-t-i-p-l-i-c-i-t-y.org/media/pdf/The-Concept-of-a-Meta-font.pdf) de Donald Knuth sur *Visible langage* ...
- et la réponse des typographes
- [Metaflop](https://www.metaflop.com/)

## Typographies réalisées avec Metapost / Metafont
- De multiples typographies et leurs sources sont disponibles sur [CTAN](https://www.ctan.org/topic/font-mf)
- L'[OCR PBI](https://gitlab.com/Luuse/foundry/OCR-PBI) de Antoine Gelgon et Lise Brosseau
- La [Meta-old-French](https://gitlab.com/Luuse/foundry/meta-old-french) de Luuse 
- Amélie Dumont

